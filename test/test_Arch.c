#include "unity.h"

#include "Arch.h"

static register16_t test_reg = 0x0000;

void setUp(void)
{
}

void tearDown(void)
{
}

void test_Arch_ReadReg16_ShouldReturnRegisterValue(void)
{
    test_reg = 0xFFFF;
    TEST_ASSERT_EQUAL_HEX16(0xFFFF, Arch_ReadReg16(&test_reg));
    
    test_reg = 0xAAAA;
    TEST_ASSERT_EQUAL_HEX16(0xAAAA, Arch_ReadReg16(&test_reg));
    
    test_reg = 0xCAFE;
    TEST_ASSERT_EQUAL_HEX16(0xCAFE, Arch_ReadReg16(&test_reg));
    
    test_reg = 0x0000;
    TEST_ASSERT_EQUAL_HEX16(0x0000, Arch_ReadReg16(&test_reg));
}

void test_Arch_WriteReg16_ShouldWriteValueToRegister(void)
{
    Arch_WriteReg16(&test_reg, 0xFFFF);
    TEST_ASSERT_EQUAL_HEX16(0xFFFF, test_reg);
    
    Arch_WriteReg16(&test_reg, 0xAAAA);
    TEST_ASSERT_EQUAL_HEX16(0xAAAA, test_reg);
    
    Arch_WriteReg16(&test_reg, 0xCAFE);
    TEST_ASSERT_EQUAL_HEX16(0xCAFE, test_reg);
    
    Arch_WriteReg16(&test_reg, 0x0000);
    TEST_ASSERT_EQUAL_HEX16(0x0000, test_reg);
}

void test_Arch_SetBitReg16_ShouldSetGivenBit(void)
{
    test_reg = 0x0000;
    
    Arch_SetBitReg16(&test_reg, 0);
    TEST_ASSERT_EQUAL_HEX16(0x0001, test_reg);
    
    Arch_SetBitReg16(&test_reg, 7);
    TEST_ASSERT_EQUAL_HEX16(0x0081, test_reg);
    
    Arch_SetBitReg16(&test_reg, 8);
    TEST_ASSERT_EQUAL_HEX16(0x0181, test_reg);
    
    Arch_SetBitReg16(&test_reg, 15);
    TEST_ASSERT_EQUAL_HEX16(0x8181, test_reg);
    
    Arch_SetBitReg16(&test_reg, 0);
    TEST_ASSERT_EQUAL_HEX16(0x8181, test_reg);
    
    Arch_SetBitReg16(&test_reg, 7);
    TEST_ASSERT_EQUAL_HEX16(0x8181, test_reg);
    
    Arch_SetBitReg16(&test_reg, 8);
    TEST_ASSERT_EQUAL_HEX16(0x8181, test_reg);
    
    Arch_SetBitReg16(&test_reg, 15);
    TEST_ASSERT_EQUAL_HEX16(0x8181, test_reg);
}

void test_Arch_ClearBitReg16_ShouldClearGivenBit(void)
{
    test_reg = 0xFFFF;
    
    Arch_ClearBitReg16(&test_reg, 0);
    TEST_ASSERT_EQUAL_HEX16(0xFFFE, test_reg);
    
    Arch_ClearBitReg16(&test_reg, 7);
    TEST_ASSERT_EQUAL_HEX16(0xFF7E, test_reg);
    
    Arch_ClearBitReg16(&test_reg, 8);
    TEST_ASSERT_EQUAL_HEX16(0xFE7E, test_reg);
    
    Arch_ClearBitReg16(&test_reg, 15);
    TEST_ASSERT_EQUAL_HEX16(0x7E7E, test_reg);
    
    Arch_ClearBitReg16(&test_reg, 0);
    TEST_ASSERT_EQUAL_HEX16(0x7E7E, test_reg);
    
    Arch_ClearBitReg16(&test_reg, 7);
    TEST_ASSERT_EQUAL_HEX16(0x7E7E, test_reg);
    
    Arch_ClearBitReg16(&test_reg, 8);
    TEST_ASSERT_EQUAL_HEX16(0x7E7E, test_reg);
    
    Arch_ClearBitReg16(&test_reg, 15);
    TEST_ASSERT_EQUAL_HEX16(0x7E7E, test_reg);
}

void test_Arch_ToggleBitReg16_ShouldToggleGivenBit(void)
{
    test_reg = 0x0000;
    
    Arch_ToggleBitReg16(&test_reg, 0);
    TEST_ASSERT_EQUAL_HEX16(0x0001, test_reg);
    
    Arch_ToggleBitReg16(&test_reg, 7);
    TEST_ASSERT_EQUAL_HEX16(0x0081, test_reg);
    
    Arch_ToggleBitReg16(&test_reg, 8);
    TEST_ASSERT_EQUAL_HEX16(0x0181, test_reg);
    
    Arch_ToggleBitReg16(&test_reg, 15);
    TEST_ASSERT_EQUAL_HEX16(0x8181, test_reg);
    
    Arch_ToggleBitReg16(&test_reg, 0);
    TEST_ASSERT_EQUAL_HEX16(0x8180, test_reg);
    
    Arch_ToggleBitReg16(&test_reg, 7);
    TEST_ASSERT_EQUAL_HEX16(0x8100, test_reg);
    
    Arch_ToggleBitReg16(&test_reg, 8);
    TEST_ASSERT_EQUAL_HEX16(0x8000, test_reg);
    
    Arch_ToggleBitReg16(&test_reg, 15);
    TEST_ASSERT_EQUAL_HEX16(0x0000, test_reg);
}

void test_Arch_ReadBitReg16_ShouldReturnGivenBitValue(void)
{
    test_reg = 0xFFFE;
    TEST_ASSERT_EQUAL_HEX16(0, Arch_ReadBitReg16(&test_reg, 0));
    test_reg = 0x0001;
    TEST_ASSERT_EQUAL_HEX16(1, Arch_ReadBitReg16(&test_reg, 0));
    
    test_reg = 0xFF7F;
    TEST_ASSERT_EQUAL_HEX16(0, Arch_ReadBitReg16(&test_reg, 7));
    test_reg = 0x0080;
    TEST_ASSERT_EQUAL_HEX16(1, Arch_ReadBitReg16(&test_reg, 7));
    
    test_reg = 0xFEFF;
    TEST_ASSERT_EQUAL_HEX16(0, Arch_ReadBitReg16(&test_reg, 8));
    test_reg = 0x0100;
    TEST_ASSERT_EQUAL_HEX16(1, Arch_ReadBitReg16(&test_reg, 8));
    
    test_reg = 0x7FFF;
    TEST_ASSERT_EQUAL_HEX16(0, Arch_ReadBitReg16(&test_reg, 15));
    test_reg = 0x8000;
    TEST_ASSERT_EQUAL_HEX16(1, Arch_ReadBitReg16(&test_reg, 15));
}

void test_Arch_WriteBitReg16_ShouldWriteValueToGivenBit(void)
{
    test_reg = 0xFFFF;
    Arch_WriteBitReg16(&test_reg, 0, 0);
    TEST_ASSERT_EQUAL_HEX16(0xFFFE, test_reg);
    test_reg = 0x0000;
    Arch_WriteBitReg16(&test_reg, 0, 1);
    TEST_ASSERT_EQUAL_HEX16(0x0001, test_reg);
    
    test_reg = 0xFFFF;
    Arch_WriteBitReg16(&test_reg, 7, 0);
    TEST_ASSERT_EQUAL_HEX16(0xFF7F, test_reg);
    test_reg = 0x0000;
    Arch_WriteBitReg16(&test_reg, 7, 1);
    TEST_ASSERT_EQUAL_HEX16(0x0080, test_reg);
    
    test_reg = 0xFFFF;
    Arch_WriteBitReg16(&test_reg, 8, 0);
    TEST_ASSERT_EQUAL_HEX16(0xFEFF, test_reg);
    test_reg = 0x0000;
    Arch_WriteBitReg16(&test_reg, 8, 1);
    TEST_ASSERT_EQUAL_HEX16(0x0100, test_reg);
    
    test_reg = 0xFFFF;
    Arch_WriteBitReg16(&test_reg, 15, 0);
    TEST_ASSERT_EQUAL_HEX16(0x7FFF, test_reg);
    test_reg = 0x0000;
    Arch_WriteBitReg16(&test_reg, 15, 1);
    TEST_ASSERT_EQUAL_HEX16(0x8000, test_reg);
}
