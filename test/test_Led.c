#include "unity.h"

#include "Led.h"
#include "mock_Arch.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_Led_Init_ShouldSetupPinAsDigitalOutputAndSetLow(void)
{
	Arch_ClearBitReg16_Expect(&ANSELB, 4);
	Arch_ClearBitReg16_Expect(&TRISB, 4);
	Arch_ClearBitReg16_Expect(&LATB, 4);
	
	Led_Init();
}

void test_Led_Toggle_ShouldToggleRespectivePin(void)
{
	Arch_ToggleBitReg16_Expect(&LATB, 4);
	
	Led_Toggle();
}
