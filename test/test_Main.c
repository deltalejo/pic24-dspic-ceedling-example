#include "unity.h"

#include "Main.h"
#include "mock_Utils.h"
#include "mock_Led.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_Main_ShouldSetupLed(void)
{
	Led_Init_Expect();
	Forever_ExpectAndReturn(0);
	
	MAIN();
}

void test_Main_ShouldToggleLedEvery500ms(void)
{
	Led_Init_Expect();
	
	Forever_ExpectAndReturn(1);
	Led_Toggle_Expect();
	Utils_Delay_ms_Expect(500);
	
	Forever_ExpectAndReturn(1);
	Led_Toggle_Expect();
	Utils_Delay_ms_Expect(500);
	
	Forever_ExpectAndReturn(0);
	
	MAIN();
}
