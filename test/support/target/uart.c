#include <xc.h>
#include <libpic30.h>
#include "uart.h"

#define FRC         (7370000UL)
#define FP          (FRC/2)
#define BAUDRATE    (115200)
#define BRGVAL      (((FP + 2*BAUDRATE)/(4*BAUDRATE)) - 1)

void uart_setup(void)
{
	__builtin_write_OSCCONL(OSCCONL & 0xBF); // unlock PPS
	
	RPOR6bits.RP54R = 0x0001;    //RC6->UART1:U1TX
	RPINR18bits.U1RXR = 0x0029;    //RB9->UART1:U1RX
	
	__builtin_write_OSCCONL(OSCCONL | 0x40); // lock PPS
	
	PMD1bits.U1MD = 0;
	
	IEC0bits.U1TXIE = 0;
	IEC0bits.U1RXIE = 0;
	
	U1MODEbits.PDSEL = 0;
	U1MODEbits.STSEL = 0;
	U1MODEbits.ABAUD = 0;
	U1MODEbits.BRGH = 1;
	U1STA = 0x00;
	U1BRG = BRGVAL;
	
	U1STAbits.UTXISEL0 = 0;
	U1STAbits.UTXISEL1 = 0;
	U1STAbits.URXISEL = 0;
	
	U1MODEbits.UARTEN = 1;
	U1STAbits.UTXEN = 1;
	
	__delay32(BAUDRATE*FP);
}
