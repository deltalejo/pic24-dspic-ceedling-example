require "optparse"
require "open3"
require "serialport"

BUFFLEN = 512

options = {:set => []}
OptionParser.new do |opts|
	opts.banner = "Usage: ruby #{File.basename($0, ".*")} [options] elf-file"
	opts.on("--device DEVICE", "Target device")
	opts.on("--hwtool HWTOOL", "Debug tool")
	opts.on("--mdb_exec MDB_EXEC", "MDB executable")
	opts.on("--port PORT", "Serial port")
	opts.on("--baud BAUDRATE", OptionParser::DecimalInteger, "Baudrate")
	opts.on("--set PROP=VAL", "Set tool option (can be given multiple times)") do |opt|
		prop_val = opt.split("=")
		if (prop_val.size != 2)
		   fail "invalid --set option"
		end
		options[:set] << {:prop => prop_val[0], :val => prop_val[1]}
	end
	opts.on_tail("-h", "--help", "Display help") do
		puts opts
		exit
	end
end.parse!(into: options)

if (ARGV.count != 1)
	fail "more arguments than expected"
else
	options[:elf_file] = ARGV.pop
end

if !options[:device]
	fail "missing target device"
end

if !options[:hwtool]
	fail "missing debug tool"
elsif (options[:hwtool] != "sim")
	fail "missing serial port" if !options[:port]
	fail "missing baudrate" if !options[:baud]
end

if !File.exist?(options[:mdb_exec])
	fail "could not find MDB executable '#{options[:mdb_exec]}'"
end

if (File.extname(options[:elf_file]) != ".elf")
	fail "expected .elf file"
elsif !File.exist?(options[:elf_file])
	fail "could not find .elf file '#{options[:elf_file]}'"
end

out_dirname = File.join(File.dirname(options[:elf_file]), "mdb")
out_basename = File.basename(options[:elf_file], ".elf")

Dir.mkdir(out_dirname) if !Dir.exist?(out_dirname)

options[:mdb_cmd] = File.join(out_dirname, "#{out_basename}_cmd.txt")
options[:mdb_stdout] = File.join(out_dirname, "#{out_basename}_stdout.txt")
options[:mdb_stderr] = File.join(out_dirname, "#{out_basename}_stderr.txt")
options[:mdb_out] = File.join(out_dirname, "#{out_basename}_out.txt")

File.delete(options[:mdb_cmd]) if File.exist?(options[:mdb_cmd])
File.delete(options[:mdb_stdout]) if File.exist?(options[:mdb_stdout])
File.delete(options[:mdb_stderr]) if File.exist?(options[:mdb_stderr])
File.delete(options[:mdb_out]) if File.exist?(options[:mdb_out])

if (options[:hwtool] == "sim")
	options[:set].insert(0, {:prop => "uart1io.output", :val => "file"})
	options[:set].insert(0, {:prop => "uart1io.uartioenabled", :val => "true"})
	options[:set].insert(0, {:prop => "uart1io.outputfile", :val => "\"#{options[:mdb_out]}\""})
end

File.open(options[:mdb_cmd], "w") do |f|
	f.puts("device #{options[:device]}")
	options[:set].each {|opt| f.puts("set #{opt[:prop]} #{opt[:val]}")}
	f.puts("hwtool #{options[:hwtool]}")
	f.puts("program #{options[:elf_file]}")
	f.puts("run")
	f.puts("wait")
	f.puts("quit")
end

Open3.popen3("#{options[:mdb_exec]}", "#{options[:mdb_cmd]}") do |mdb_i, mdb_o, mdb_e, mdb_t|
	mdb_i.close()
	threads = []
	
	threads << Thread.new do
		File.open(options[:mdb_stdout], "w") do |f|
			until mdb_o.eof? do
				data = mdb_o.readpartial(BUFFLEN)
				f.write(data) if !data.nil?
			end
		end
	end
	
	threads << Thread.new do
		File.open(options[:mdb_stderr], "w") do |f|
			until mdb_e.eof? do
				data = mdb_e.readpartial(BUFFLEN)
				f.write(data) if !data.nil?
			end
		end
	end
	
	if (options[:hwtool] != "sim")
		threads << Thread.new do
			File.open(options[:mdb_out], "w") do |f|
				SerialPort.open(options[:port], options[:baud]) do |s|
					s.read_timeout = 0
					until ((c = s.getc()) == "\x04") do
						f.write(c) if !c.nil?
					end
				end
			end
		end
	end
	
	threads.each {|thr| thr.join}
	
	mdb_result = mdb_t.value
	if !mdb_result.success?
		fail "error running '#{options[:elf_file]}'"
	end
end

File.open(options[:mdb_out], "r") do |f|
	test_results = f.read
	test_results.gsub!(/\R/, "\n")
	print test_results
end
