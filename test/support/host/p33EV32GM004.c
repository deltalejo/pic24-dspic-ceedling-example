#include <p33EV32GM004.h>

volatile uint16_t  WREG0 __attribute__((__deprecated__));
volatile uint16_t  WREG1 __attribute__((__deprecated__));
volatile uint16_t  WREG2 __attribute__((__deprecated__));
volatile uint16_t  WREG3 __attribute__((__deprecated__));
volatile uint16_t  WREG4 __attribute__((__deprecated__));
volatile uint16_t  WREG5 __attribute__((__deprecated__));
volatile uint16_t  WREG6 __attribute__((__deprecated__));
volatile uint16_t  WREG7 __attribute__((__deprecated__));
volatile uint16_t  WREG8 __attribute__((__deprecated__));
volatile uint16_t  WREG9 __attribute__((__deprecated__));
volatile uint16_t  WREG10 __attribute__((__deprecated__));
volatile uint16_t  WREG11 __attribute__((__deprecated__));
volatile uint16_t  WREG12 __attribute__((__deprecated__));
volatile uint16_t  WREG13 __attribute__((__deprecated__));
volatile uint16_t  WREG14 __attribute__((__deprecated__));
volatile uint16_t  WREG15 __attribute__((__deprecated__));
volatile uint16_t  SPLIM;
volatile uint16_t  ACCAL __attribute__((__deprecated__));
volatile uint16_t  ACCAH __attribute__((__deprecated__));
volatile uint8_t   ACCAU __attribute__((__deprecated__));
volatile uint16_t  ACCBL __attribute__((__deprecated__));
volatile uint16_t  ACCBH __attribute__((__deprecated__));
volatile uint8_t   ACCBU __attribute__((__deprecated__));
volatile uint16_t  PCL;
volatile uint8_t   PCH;

volatile uint16_t  DSRPAG;

volatile uint16_t  DSWPAG;

volatile uint16_t  RCOUNT;
volatile uint16_t  DCOUNT;
volatile uint16_t  DOSTARTL;
volatile uint16_t  DOSTARTH;
volatile uint16_t  DOENDL;
volatile uint16_t  DOENDH;

volatile uint16_t  SR;

volatile uint16_t  CORCON;

volatile uint16_t  MODCON;

volatile uint16_t  XMODSRT;
volatile uint16_t  XMODEND;
volatile uint16_t  YMODSRT;
volatile uint16_t  YMODEND;

volatile uint16_t  XBREV;

volatile uint16_t  DISICNT;

volatile uint16_t  TBLPAG;

volatile uint16_t  MSTRPR;

volatile uint16_t  CTXTSTAT;

volatile uint16_t  TMR1;
volatile uint16_t  PR1;

volatile uint16_t  T1CON;

volatile uint16_t  TMR2;
volatile uint16_t  TMR3HLD;
volatile uint16_t  TMR3;
volatile uint16_t  PR2;
volatile uint16_t  PR3;

volatile uint16_t  T2CON;

volatile uint16_t  T3CON;

volatile uint16_t  TMR4;
volatile uint16_t  TMR5HLD;
volatile uint16_t  TMR5;
volatile uint16_t  PR4;
volatile uint16_t  PR5;

volatile uint16_t  T4CON;

volatile uint16_t  T5CON;

volatile uint16_t  IC1CON1;

volatile uint16_t  IC1CON2;

volatile IC IC1;
volatile IC IC2;
volatile IC IC3;
volatile IC IC4;
volatile IC IC5;
volatile IC IC6;
volatile IC IC7;
volatile IC IC8;
volatile uint16_t  IC1BUF;
volatile uint16_t  IC1TMR;

volatile uint16_t  IC2CON1;

volatile uint16_t  IC2CON2;

volatile uint16_t  IC2BUF;
volatile uint16_t  IC2TMR;

volatile uint16_t  IC3CON1;

volatile uint16_t  IC3CON2;

volatile uint16_t  IC3BUF;
volatile uint16_t  IC3TMR;

volatile uint16_t  IC4CON1;

volatile uint16_t  IC4CON2;

volatile uint16_t  IC4BUF;
volatile uint16_t  IC4TMR;

volatile uint16_t  I2C1CON1;

volatile uint16_t  I2C1CONL;

volatile uint16_t  I2C1CON2;

volatile uint16_t  I2C1CONH;

volatile uint16_t  I2C1STAT;

volatile uint16_t  I2C1ADD;

volatile uint16_t  I2C1MSK;

volatile uint16_t  I2C1BRG;

volatile uint16_t  I2C1TRN;

volatile uint16_t  I2C1RCV;

volatile UART UART1;
volatile UART UART2;

volatile uint16_t  U1MODE;

volatile uint16_t  U1STA;

volatile uint16_t  U1TXREG;
volatile uint16_t  U1RXREG;
volatile uint16_t  U1BRG;

volatile uint16_t  U2MODE;

volatile uint16_t  U2STA;

volatile uint16_t  U2TXREG;
volatile uint16_t  U2RXREG;
volatile uint16_t  U2BRG;
volatile SPI SPI1;
volatile SPI SPI2;

volatile uint16_t  SPI1STAT;

volatile uint16_t  SPI1CON1;

volatile uint16_t  SPI1CON2;

volatile uint16_t  SPI1BUF;

volatile uint16_t  SPI2STAT;

volatile uint16_t  SPI2CON1;

volatile uint16_t  SPI2CON2;

volatile uint16_t  SPI2BUF;
volatile uint16_t  ADC1BUF0;
volatile uint16_t  ADC1BUF1;
volatile uint16_t  ADC1BUF2;
volatile uint16_t  ADC1BUF3;
volatile uint16_t  ADC1BUF4;
volatile uint16_t  ADC1BUF5;
volatile uint16_t  ADC1BUF6;
volatile uint16_t  ADC1BUF7;
volatile uint16_t  ADC1BUF8;
volatile uint16_t  ADC1BUF9;
volatile uint16_t  ADC1BUFA;
volatile uint16_t  ADC1BUFB;
volatile uint16_t  ADC1BUFC;
volatile uint16_t  ADC1BUFD;
volatile uint16_t  ADC1BUFE;
volatile uint16_t  ADC1BUFF;

volatile uint16_t  AD1CON1;

volatile uint16_t  AD1CON2;

volatile uint16_t  AD1CON3;

volatile uint16_t  AD1CHS123;

volatile uint16_t  AD1CHS0;

volatile uint16_t  AD1CSSH;

volatile uint16_t  AD1CSSL;

volatile uint16_t  AD1CON4;

volatile uint16_t  CTMUCON1;

volatile uint16_t  CTMUCON2;

volatile uint16_t  CTMUICON;

volatile uint16_t  SENT1CON1;

volatile uint16_t  SENT1CON2;
volatile uint16_t  SENT1CON3;

volatile uint16_t  SENT1STAT;

volatile uint16_t  SENT1SYNC;

volatile uint16_t  SENT1DATL;

volatile uint16_t  SENT1DATH;

volatile uint16_t  SENT2CON1;

volatile uint16_t  SENT2CON2;
volatile uint16_t  SENT2CON3;

volatile uint16_t  SENT2STAT;

volatile uint16_t  SENT2SYNC;

volatile uint16_t  SENT2DATL;

volatile uint16_t  SENT2DATH;

volatile uint16_t  RPOR0;

volatile uint16_t  RPOR1;

volatile uint16_t  RPOR2;

volatile uint16_t  RPOR3;

volatile uint16_t  RPOR4;

volatile uint16_t  RPOR5;

volatile uint16_t  RPOR6;

volatile uint16_t  RPOR7;

volatile uint16_t  RPOR10;

volatile uint16_t  RPOR11;

volatile uint16_t  RPOR12;

volatile uint16_t  RPOR13;

volatile uint16_t  RPINR0;

volatile uint16_t  RPINR1;

volatile uint16_t  RPINR3;

volatile uint16_t  RPINR7;

volatile uint16_t  RPINR8;

volatile uint16_t  RPINR11;

volatile uint16_t  RPINR12;

volatile uint16_t  RPINR18;

volatile uint16_t  RPINR19;

volatile uint16_t  RPINR22;

volatile uint16_t  RPINR23;

volatile uint16_t  RPINR37;

volatile uint16_t  RPINR38;

volatile uint16_t  RPINR39;

volatile uint16_t  RPINR44;

volatile uint16_t  RPINR45;

volatile uint16_t  DMTCON;

volatile uint16_t  DMTPRECLR;

volatile uint16_t  DMTCLR;

volatile uint16_t  DMTSTAT;

volatile uint16_t  DMTCNTL;
volatile uint16_t  DMTCNTH;
volatile uint16_t  DMTHOLDREG;
volatile uint16_t  DMTPSCNTL;
volatile uint16_t  DMTPSCNTH;
volatile uint16_t  DMTPSINTVL;
volatile uint16_t  DMTPSINTVH;

volatile uint16_t  NVMCON;

volatile uint16_t  NVMADR;

volatile uint16_t  NVMADRU;

volatile uint16_t  NVMKEY;
volatile uint16_t  NVMSRCADRL;

volatile uint16_t  NVMSRCADRH;

volatile uint16_t  RCON;

volatile uint16_t  OSCCON;

volatile uint8_t OSCCONL;
volatile uint8_t OSCCONH;

volatile uint16_t  CLKDIV;

volatile uint16_t  PLLFBD;

volatile uint16_t  OSCTUN;

volatile uint16_t  REFOCON;

volatile uint16_t  PMD1;

volatile uint16_t  PMD2;

volatile uint16_t  PMD3;

volatile uint16_t  PMD4;

volatile uint16_t  PMD6;

volatile uint16_t  PMD7;

volatile uint16_t  PMD8;

volatile uint16_t  IFS0;

volatile uint16_t  IFS1;

volatile uint16_t  IFS2;

volatile uint16_t  IFS3;

volatile uint16_t  IFS4;

volatile uint16_t  IFS5;

volatile uint16_t  IFS6;

volatile uint16_t  IFS8;

volatile uint16_t  IFS10;

volatile uint16_t  IFS11;

volatile uint16_t  IEC0;

volatile uint16_t  IEC1;

volatile uint16_t  IEC2;

volatile uint16_t  IEC3;

volatile uint16_t  IEC4;

volatile uint16_t  IEC5;

volatile uint16_t  IEC6;

volatile uint16_t  IEC8;

volatile uint16_t  IEC10;

volatile uint16_t  IEC11;

volatile uint16_t  IPC0;

volatile uint16_t  IPC1;

volatile uint16_t  IPC2;

volatile uint16_t  IPC3;

volatile uint16_t  IPC4;

volatile uint16_t  IPC5;

volatile uint16_t  IPC6;

volatile uint16_t  IPC7;

volatile uint16_t  IPC8;

volatile uint16_t  IPC9;

volatile uint16_t  IPC14;

volatile uint16_t  IPC16;

volatile uint16_t  IPC19;

volatile uint16_t  IPC23;

volatile uint16_t  IPC24;

volatile uint16_t  IPC35;

volatile uint16_t  IPC43;

volatile uint16_t  IPC45;

volatile uint16_t  IPC46;

volatile uint16_t  INTCON1;

volatile uint16_t  INTCON2;

volatile uint16_t  INTCON3;

volatile uint16_t  INTCON4;

volatile uint16_t  INTTREG;

volatile uint16_t  OC1CON1;

volatile uint16_t  OC1CON2;

volatile OC OC1;
volatile OC OC2;
volatile OC OC3;
volatile OC OC4;
volatile OC OC5;
volatile OC OC6;
volatile OC OC7;
volatile OC OC8;
volatile uint16_t  OC1RS;
volatile uint16_t  OC1R;
volatile uint16_t  OC1TMR;

volatile uint16_t  OC2CON1;

volatile uint16_t  OC2CON2;

volatile uint16_t  OC2RS;
volatile uint16_t  OC2R;
volatile uint16_t  OC2TMR;

volatile uint16_t  OC3CON1;

volatile uint16_t  OC3CON2;

volatile uint16_t  OC3RS;
volatile uint16_t  OC3R;
volatile uint16_t  OC3TMR;

volatile uint16_t  OC4CON1;

volatile uint16_t  OC4CON2;

volatile uint16_t  OC4RS;
volatile uint16_t  OC4R;
volatile uint16_t  OC4TMR;

volatile uint16_t  CMSTAT;

volatile uint16_t  CVR1CON;

volatile uint16_t  CM1CON;

volatile uint16_t  CM1MSKSRC;

volatile uint16_t  CM1MSKCON;

volatile uint16_t  CM1FLTR;

volatile uint16_t  CM2CON;

volatile uint16_t  CM2MSKSRC;

volatile uint16_t  CM2MSKCON;

volatile uint16_t  CM2FLTR;

volatile uint16_t  CM3CON;

volatile uint16_t  CM3MSKSRC;

volatile uint16_t  CM3MSKCON;

volatile uint16_t  CM3FLTR;

volatile uint16_t  CM4CON;

volatile uint16_t  CM4MSKSRC;

volatile uint16_t  CM4MSKCON;

volatile uint16_t  CM4FLTR;

volatile uint16_t  CM5CON;

volatile uint16_t  CM5MSKSRC;

volatile uint16_t  CM5MSKCON;

volatile uint16_t  CM5FLTR;

volatile uint16_t  CVR2CON;

volatile uint16_t  DMA0CON;

volatile uint16_t  DMA0REQ;

volatile uint16_t  DMA0STAL;

volatile uint16_t  DMA0STAH;

volatile uint16_t  DMA0STBL;

volatile uint16_t  DMA0STBH;

volatile uint16_t  DMA0PAD;

volatile uint16_t  DMA0CNT;

volatile uint16_t  DMA1CON;

volatile uint16_t  DMA1REQ;

volatile uint16_t  DMA1STAL;

volatile uint16_t  DMA1STAH;

volatile uint16_t  DMA1STBL;

volatile uint16_t  DMA1STBH;

volatile uint16_t  DMA1PAD;

volatile uint16_t  DMA1CNT;

volatile uint16_t  DMA2CON;

volatile uint16_t  DMA2REQ;

volatile uint16_t  DMA2STAL;

volatile uint16_t  DMA2STAH;

volatile uint16_t  DMA2STBL;

volatile uint16_t  DMA2STBH;

volatile uint16_t  DMA2PAD;

volatile uint16_t  DMA2CNT;

volatile uint16_t  DMA3CON;

volatile uint16_t  DMA3REQ;

volatile uint16_t  DMA3STAL;

volatile uint16_t  DMA3STAH;

volatile uint16_t  DMA3STBL;

volatile uint16_t  DMA3STBH;

volatile uint16_t  DMA3PAD;

volatile uint16_t  DMA3CNT;

volatile uint16_t  DMAPWC;

volatile uint16_t  DMARQC;

volatile uint16_t  DMAPPS;

volatile uint16_t  DMALCA;

volatile uint16_t  DSADRL;

volatile uint16_t  DSADRH;

volatile uint16_t  PTCON;

volatile uint16_t  PTCON2;

volatile uint16_t  PTPER;
volatile uint16_t  SEVTCMP;
volatile uint16_t  MDC;

volatile uint16_t  CHOP;

volatile uint16_t  PWMKEY;

volatile uint16_t  PWMCON1;

volatile uint16_t  IOCON1;

volatile uint16_t  FCLCON1;

volatile uint16_t  PDC1;
volatile uint16_t  PHASE1;
volatile uint16_t  DTR1;
volatile uint16_t  ALTDTR1;

volatile uint16_t  TRIG1;

volatile uint16_t  TRGCON1;

volatile uint16_t  PWMCAP1;

volatile uint16_t  LEBCON1;

volatile uint16_t  LEBDLY1;

volatile uint16_t  AUXCON1;

volatile uint16_t  PWMCON2;

volatile uint16_t  IOCON2;

volatile uint16_t  FCLCON2;

volatile uint16_t  PDC2;
volatile uint16_t  PHASE2;
volatile uint16_t  DTR2;
volatile uint16_t  ALTDTR2;

volatile uint16_t  TRIG2;

volatile uint16_t  TRGCON2;

volatile uint16_t  PWMCAP2;

volatile uint16_t  LEBCON2;

volatile uint16_t  LEBDLY2;

volatile uint16_t  AUXCON2;

volatile uint16_t  PWMCON3;

volatile uint16_t  IOCON3;

volatile uint16_t  FCLCON3;

volatile uint16_t  PDC3;
volatile uint16_t  PHASE3;
volatile uint16_t  DTR3;
volatile uint16_t  ALTDTR3;

volatile uint16_t  TRIG3;

volatile uint16_t  TRGCON3;

volatile uint16_t  PWMCAP3;

volatile uint16_t  LEBCON3;

volatile uint16_t  LEBDLY3;

volatile uint16_t  AUXCON3;

volatile uint16_t  TRISA;

volatile uint16_t  PORTA;

volatile uint16_t  LATA;

volatile uint16_t  ODCA;

volatile uint16_t  CNENA;

volatile uint16_t  CNPUA;

volatile uint16_t  CNPDA;

volatile uint16_t  ANSELA;

volatile uint16_t  SR1A;

volatile uint16_t  SR0A;

volatile uint16_t  TRISB;

volatile uint16_t  PORTB;

volatile uint16_t  LATB;

volatile uint16_t  ODCB;

volatile uint16_t  CNENB;

volatile uint16_t  CNPUB;

volatile uint16_t  CNPDB;

volatile uint16_t  ANSELB;

volatile uint16_t  SR1B;

volatile uint16_t  SR0B;

volatile uint16_t  TRISC;

volatile uint16_t  PORTC;

volatile uint16_t  LATC;

volatile uint16_t  ODCC;

volatile uint16_t  CNENC;

volatile uint16_t  CNPUC;

volatile uint16_t  CNPDC;

volatile uint16_t  ANSELC;

volatile uint16_t  SR1C;

volatile uint16_t  SR0C;

volatile uint16_t  FEXL;

volatile uint16_t  FEXU;

volatile uint16_t  FEX2L;

volatile uint16_t  FEX2U;

volatile uint16_t  VISI;
volatile uint16_t  DPCL;

volatile uint16_t  DPCH;

volatile uint16_t  APPO;
volatile uint16_t  APPI;

volatile uint16_t  APPS;

volatile uint16_t  STROUTL;
volatile uint16_t  STROUTH;
volatile uint16_t  STROVCNT;
