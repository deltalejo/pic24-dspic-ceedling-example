#ifndef XC_H
#define XC_H

#define Nop()    __builtin_nop()
#define ClrWdt() __builtin_clrwdt()
#define Sleep()  __builtin_pwrsav(0)
#define Idle()   __builtin_pwrsav(1)

#if defined(__dsPIC33EV32GM004__)
#include <p33EV32GM004.h>
#else
#error "Processor macro is not defined."
#endif

#endif /* XC_H */
