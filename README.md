Work in progress...

# PIC24-dsPIC Ceedling Template
Basic setup to implement TDD on Microchip's 16-bit devices using [Ceedling](https://www.throwtheswitch.org/ceedling). It allows you to run tests on host, simulator and target (with UART).

## Install Gems
```
gem install ceedling serialport
```

## Run tests

### Host
```
ceedling test:all
```

### Simulator
```
ceedling project:simulator test:all
```

### Target
So, you really want to mess with this, uh?
```
ceedling project:target options:[hwtool] test:all
```
