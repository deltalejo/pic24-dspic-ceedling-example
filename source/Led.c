#include <Arch.h>
#include <Led.h>

void Led_Init(void)
{
	Arch_ClearBitReg16(&ANSELB, 4);
	Arch_ClearBitReg16(&TRISB, 4);
	Arch_ClearBitReg16(&LATB, 4);
}

void Led_Toggle(void)
{
	Arch_ToggleBitReg16(&LATB, 4);
}
