#ifndef ARCH_H
#define ARCH_H

#include <stdint.h>
#include <xc.h>

typedef uint16_t volatile register16_t; // 16-bit register type
typedef unsigned int bitn_t; // Bit number type

static inline uint16_t Arch_ReadReg16(register16_t const * const _r)
{
	return (*_r);
}

static inline void Arch_WriteReg16(register16_t * const _r, uint16_t const _v)
{
	(*_r) = (_v);
}

static inline void Arch_SetBitReg16(register16_t * const _r, bitn_t const _b)
{
	const uint16_t _m = (1U << (_b));
	
	(*_r) |= (_m);
}

static inline void Arch_ClearBitReg16(register16_t * const _r, bitn_t const _b)
{
	const uint16_t _m = (1U << (_b));
	
	(*_r) &= ~(_m);
}

static inline void Arch_ToggleBitReg16(register16_t * const _r, bitn_t const _b)
{
	const uint16_t _m = (1U << (_b));
	
	(*_r) ^= (_m);
}

static inline uint16_t Arch_ReadBitReg16(register16_t const * const _r, bitn_t const _b)
{
	const uint16_t _m = (1U << (_b));
	
	return ((*_r) & (_m))? 1U : 0U;
}

static inline void Arch_WriteBitReg16(register16_t * const _r, bitn_t const _b, uint16_t const _v)
{
	if (_v) {
		Arch_SetBitReg16(_r, _b);
	}
	else {
		Arch_ClearBitReg16(_r, _b);
	}
}

#endif // ARCH_H
