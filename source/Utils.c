#include <libpic30.h>
#include <Utils.h>

#define FRC    (7370000UL)
#define FP     (FRC/2)

void Utils_Delay_ms(unsigned long ms)
{
	__delay32((ms*FP)/1000);
}
