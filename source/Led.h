#ifndef LED_H
#define LED_H

void Led_Init(void);
void Led_Toggle(void);

#endif // LED_H
