#include <Utils.h>
#include <Led.h>
#include <Main.h>

int MAIN(void)
{
	Led_Init();
	
	while (FOREVER()) {
		Led_Toggle();
		Utils_Delay_ms(500);
	}
	
	return 0;
}
