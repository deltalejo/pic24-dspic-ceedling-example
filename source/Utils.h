#ifndef UTILS_H
#define UTILS_H

#ifndef TEST
#define FOREVER()    1
#else
#define FOREVER()    Forever()
int Forever(void);
#endif

void Utils_Delay_ms(unsigned long ms);

#endif // UTILS_H
